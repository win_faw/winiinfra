package com.winitech.infra.app.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/*******************************
 * 기본적인 header, msg 형식의 반환
 * ****************************/

@Data
@AllArgsConstructor
public class DefaultResponse {
	private String header;
	private Object msg;
}
