package com.winitech.infra.app.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import com.winitech.infra.app.model.request.loginUserRequestDTO;
import com.winitech.infra.app.model.response.getUserResponseDTO;

@Service
@Mapper
public interface UserManagementMapper {
	//로그인을 통해 회원정보 가져오기
	getUserResponseDTO getUserInfo(loginUserRequestDTO user);
}
