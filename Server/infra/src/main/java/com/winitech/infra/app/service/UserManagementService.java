package com.winitech.infra.app.service;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.winitech.infra.app.model.request.loginUserRequestDTO;
import com.winitech.infra.app.model.response.DefaultResponse;
import com.winitech.infra.app.model.response.getUserResponseDTO;
import com.winitech.infra.app.mybatis.mapper.UserManagementMapper;


@Service
public class UserManagementService {
	private static final Logger log = LoggerFactory.getLogger(UserManagementService.class);
	
	@Resource
	UserManagementMapper userManagementMapper;
	
	//로그인
	public DefaultResponse loginUser(loginUserRequestDTO user) {
		getUserResponseDTO userInfo = userManagementMapper.getUserInfo(user);				
		
		return userInfo == null ? new DefaultResponse("2", "해당하는 유저가 없습니다.") : new DefaultResponse("0", userInfo);		
	}
	
	

}
