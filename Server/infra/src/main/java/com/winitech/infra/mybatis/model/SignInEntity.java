package com.winitech.infra.mybatis.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SignInEntity {
	private String userUSN;
	private String userPW;
	private String userName;
	private String userTell;
	private String userDepartmentCode;
	private String userAdminYN;
	private String userUsedYN;
	private String userLastChange;

}
