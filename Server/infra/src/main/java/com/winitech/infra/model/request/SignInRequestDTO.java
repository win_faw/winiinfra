package com.winitech.infra.model.request;

import com.winitech.infra.mybatis.model.SignInEntity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SignInRequestDTO {
	private String no;
	private String userUSN;
	private String userPW;
	private String userName;
	private String userTell;
	private String userDepartmentCode;
	private String userAdminYN;
	private String userUsedYN;
	//private String userLastChange;
	
//	public SignInEntity SignInEntity() {
//		return new SignInEntity(userUSN, userPW, userName, 
//								userTell, userDepartmentCode,
//								userAdminYN, userUsedYN);
//	}
}
