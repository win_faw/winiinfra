package com.winitech.infra.controller;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RestException{
	private String status;
	private String message;
}
