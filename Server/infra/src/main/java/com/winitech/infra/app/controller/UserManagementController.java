package com.winitech.infra.app.controller;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.winitech.infra.app.model.request.loginUserRequestDTO;
import com.winitech.infra.app.model.response.DefaultResponse;
import com.winitech.infra.app.service.UserManagementService;

import lombok.extern.java.Log;

@RestController
@Log
public class UserManagementController {
	
	@Resource
	UserManagementService userManagementService;
	
	//로그인
	@PostMapping(value = "/login", produces = "application/json; charset=UTF-8")
	public DefaultResponse getLoginInfo(@Valid @RequestBody loginUserRequestDTO user) throws Exception{
		log.info("입력값 >> " + user.getUsn() + " ::: " + user.getPw());
		
		return userManagementService.loginUser(user);		
	}
	
	//로그아웃
	/*
	 * @PostMapping(value = "/logout", produces = "application/json; charset=UTF-8")
	 * public DefaultResponse setLogout(@Valid @RequestBody logoutUserRequestDTO
	 * user) throws Exception{ log.info("입력값 >> " + user.getUsn()); return
	 * userManagementService.logoutUser(user); }
	 */

}
