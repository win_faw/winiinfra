package com.winitech.infra.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DefaultResponse2 {
	private boolean result;

}
