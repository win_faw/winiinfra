package com.winitech.infra.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginResponseDTO{	
	private String name;
	private String corporateTitle;
	private String adminYN;
	private String usedYN;
	private String phone;	
}
