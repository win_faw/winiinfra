package com.winitech.infra.app.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class loginUserRequestDTO {
	private String usn;
	private String pw;
}
