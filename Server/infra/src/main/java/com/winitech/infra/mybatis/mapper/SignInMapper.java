package com.winitech.infra.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import com.winitech.infra.model.request.SignInRequestDTO;
import com.winitech.infra.model.response.DefaultResponse2;
import com.winitech.infra.mybatis.model.SignInEntity;

@Service
@Mapper
public interface SignInMapper {
	int signIn(SignInRequestDTO user);
}
