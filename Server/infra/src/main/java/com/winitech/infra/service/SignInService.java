package com.winitech.infra.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.winitech.infra.model.request.SignInRequestDTO;
import com.winitech.infra.model.response.DefaultResponse2;
import com.winitech.infra.mybatis.mapper.SignInMapper;

@Service
public class SignInService {
	private static final Logger log = LoggerFactory.getLogger(SignInService.class);
	
	@Autowired
	SignInMapper signMapper;
	
	//insert 경우는 selectKey를 통해서 입력된 결과값이 1이면 성공 응답 0이면 실패 응답
	public DefaultResponse2 SignInService(SignInRequestDTO request) {
		return signMapper.signIn(request) == 1 ? new DefaultResponse2(true) : new DefaultResponse2(false);
	}
	
	

}
