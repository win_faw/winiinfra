package com.winitech.infra.controller;

import java.sql.SQLException;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.winitech.infra.model.request.SignInRequestDTO;
import com.winitech.infra.service.SignInService;

import lombok.extern.java.Log;

@RestController
@Log
public class SignInController {
	@Resource
	SignInService signInService;

    @GetMapping(value="/signin", produces = "application/json; charset=UTF-8")
    public Object insertUserInfo(@Valid @RequestBody SignInRequestDTO request) throws SQLException{
    	log.info("요청값 >>" + request.toString());
    	
    	try {
    		return signInService.SignInService(request);
    	}catch(Exception e){    		
    		// 예외 코드
    		HashMap<String, String> result = new HashMap<>();
    		result.put("header", "2");
    		result.put("msg", e.toString());
    		return result;
    	}
    }

}
