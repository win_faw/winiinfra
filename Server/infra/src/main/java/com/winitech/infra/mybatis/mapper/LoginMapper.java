package com.winitech.infra.mybatis.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import com.winitech.infra.model.response.LoginResponseDTO;
import com.winitech.infra.mybatis.model.LoginEntity;

@Service
@Mapper
public interface LoginMapper {
	//usn과 pw를 입력하여 login 시도
	LoginResponseDTO login(LoginEntity loginEntity);
}
