package com.winitech.infra.mybatis.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginEntity {
	private String usn;
	private String pw;
}
