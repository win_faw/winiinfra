package com.winitech.infra.model.request;

import com.winitech.infra.mybatis.model.LoginEntity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginRequestDTO {
	private String usn;
	private String pw;
	
	// 로그인 entity생성
	public LoginEntity loginEntity() {
        return new LoginEntity(usn, pw);
    }

}
