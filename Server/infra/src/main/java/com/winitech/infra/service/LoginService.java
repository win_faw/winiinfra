package com.winitech.infra.service;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.winitech.infra.controller.RestException;
import com.winitech.infra.model.request.LoginRequestDTO;
import com.winitech.infra.model.response.LoginResponseDTO;
import com.winitech.infra.mybatis.mapper.LoginMapper;

@Service
public class LoginService {
	private static final Logger log = LoggerFactory.getLogger(LoginService.class);
	
	@Autowired
	LoginMapper loginMapper;
	
	
	public Object LoginService(LoginRequestDTO request) {
		if(loginMapper.login(request.loginEntity()) == null) {
//			LoginResponseDTO result = new LoginResponseDTO(null, null, null, null, null);
//			return result;
			HashMap<String, String> result = new HashMap<>();					
			result.put("msg", "검색된 결과 값이 없습니다.");
			result.put("header", "1");
			return result;
			
		}else {
			HashMap<String, Object> result = new HashMap<>();
			result.put("header", "0");
			result.put("msg", loginMapper.login(request.loginEntity()));
			
			return result;
		
//			return loginMapper.login(request.loginEntity());
		}
    }
	
}
