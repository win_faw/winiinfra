package com.winitech.infra.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.winitech.infra.model.request.LoginRequestDTO;
import com.winitech.infra.model.response.LoginResponseDTO;
import com.winitech.infra.service.LoginService;

import lombok.extern.java.Log;

@RestController
@Log
public class LoginController {

	@ResponseBody
	@GetMapping("/test")
	public String test() {
		return "hi";
	}
	
	@Resource
	LoginService loginService;

	
//	//@ApiOperation(value="Sample Controller Test")
//    @PostMapping(value = "/login", produces = "application/json;charset=UTF-8")    
//    public Object getLoginInfo(@Valid @RequestBody LoginRequestDTO request) throws Exception{
//    	//request.setUsn("588");
//		//request.setPw("1233");
//    	
//    	log.info("USN은 >>" + request.getUsn());
//    	log.info("PW는 >>" + request.getPw());   	
//    	
//    	try {    		
//    		return loginService.LoginService(request);
//    	}catch(Exception e){
//    		log.info("예외처리 >>" + e.toString());
//    		throw new SampleException();
//    	}    	    	
//    }
}
