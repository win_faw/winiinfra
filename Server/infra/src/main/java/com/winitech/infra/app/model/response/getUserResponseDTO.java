package com.winitech.infra.app.model.response;

import lombok.Data;

@Data
public class getUserResponseDTO {
	private String userUsn;
	private String userPw;
	private String userName;
	private String userTell;
	private String departmentName;
	private String userAdminYN;
	private String userUsedYN;
	private String userLastChange;
}
