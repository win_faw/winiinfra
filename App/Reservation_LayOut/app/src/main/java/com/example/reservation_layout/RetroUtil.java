package com.example.reservation_layout;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroUtil {
    //@GET방식 DATA가져올꺼
    private Retrofit2 mGetData;
    //생성자 만들어서
    public  RetroUtil() {
        // Builder 만들어서 BACE-URL Retrofit2 인터페이스 안에 지정해 둔거 사용
        Retrofit mRetrofit = new Retrofit.Builder()
                .baseUrl(Retrofit2.URL)
                //컨버터를 이용해서 json 파싱
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mGetData = mRetrofit.create(Retrofit2.class);
    }
    //외부에서 가져다 쓰는 법 위 방법을 매번 쓰지 말고 클래스 만들어서 사용가능
    public Retrofit2 getmGetData() {
        return mGetData;
    }
}
