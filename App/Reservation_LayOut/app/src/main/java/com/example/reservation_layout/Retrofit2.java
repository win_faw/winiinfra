package com.example.reservation_layout;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Retrofit2 {
    String URL = "http://54.180.159.207";

    //body는 내가 보낼값
    @POST("/login")
    Call<TransData> getTransData(@Body TransData transData);

    //올바른 정보가 전달됬을 때 다시 받아올 값
    @GET("/login")
    Call<RecevData> getRecevData(
            @Query("name") String name, @Query("corperateTitle") String corperateTitle,
            @Query("adminYN") String adminYN, @Query("usedYN") String usedYN,
            @Query("phone") String phone);

//    @FormUrlEncoded
//    @POST
//    Call<RecevData> getRecevData(@FieldMap HashMap<String, Object> Param);

}
